---
layout: job_family_page
title: "Frontend Engineer - Marketing"
---
## Job Grade 

The Frontend Engineer - Marketing is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Frontend Engineering Roles on the Marketing team at GitLab

Frontend Engineers - Marketing at GitLab work on our marketing website: about.GitLab.com. Frontend Engineers - Marketing work closely with product marketing, content marketing, and other members of the Marketing team. This role will be part of the Growth Marketing Digital Experience team.

Unless otherwise specified, all Frontend Engineering roles at GitLab share the following requirements and responsibilities:

## Requirements

- Expert knowledge of HTML, CSS, HAML and JavaScript (jQuery, Vue.js).
- Understanding of responsive design and best practices.
- The ability to iterate quickly and embrace feedback from many perspectives.
- Knowledge of information architecture, interaction design, and user-centered design.
- Experience using Git in a professional/workplace environment
- A solid understanding in core web and browser concepts (eg. how the browser parses and constructs a web page)
- Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment
- Demonstrated capacity to clearly and concisely communicate complex technical, architectural, and/or organizational problems and propose thorough iterative solutions
- Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems
- Comfort working in an agile, [iterative](https://about.gitlab.com/handbook/values/#iteration) development process
- Positive and solution-oriented mindset
- Effective communication skills: [Regularly achieve consensus with peers](https://about.gitlab.com/handbook/values/#collaboration), and clear status updates
- An inclination towards communication, inclusion, and visibility
- [Self-motivated and self-managing](https://about.gitlab.com/handbook/values/#efficiency), with great organizational skills.
- Share [our values](https://about.gitlab.com/handbook/values/), and work in accordance with those values
- Ability to thrive in a fully remote organization
- Ability to use GitLab

## Nice to have:

- Experience with JAMstack, Ruby, and Middleman (and/or other static site generators).
- Experience with the GitLab product as a user or contributor
- SaaS Product company experience
- Experience working with a remote team
- Enterprise software Marketing experience
- Experience working with a global or otherwise multicultural team

## Responsibilities

- Collaborate with team members on the simplest solution to problems
- Develop features and enhancements to GitLab's marketing site in a secure, well-tested, and performant way
- You’ll work with the Growth Marketing team and other stakeholders (Content Strategy, Brand, etc.) to iterate on new features and improvements to GitLab's marketing site
- Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
- Consistently ship small features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.
- You'll help improve the overall experience of our Marketing website through improving the quality of the Frontend features both in your [group](https://about.gitlab.com/handbook/product/product-categories/#hierarchy) and features that benefit other groups.
- You’ll help identify areas of improvements in the code base, both specific to your [group](https://about.gitlab.com/handbook/product/product-categories/#hierarchy) and outside your group (eg. component library) and help contribute to make it better
- You’ll learn, collaborate and teach other Frontend Engineers. Everyone can contribute something new to the team regardless of how long they’ve been in the industry.
- You’ll collaboratively plan your sprints and complete prioritized issues from the issue tracker.

## Performance Indicators

Frontend Engineers have the following job-family performance indicators.

- [Contributing to the success of Marketing's Quarterly Initiatives](https://about.gitlab.com/handbook/marketing/growth-marketing/#q3-fy21-initiatives)
- [Identifying and organizing epics into executable Sprint plans](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#sprint-planning)
- [Successfully completing weekly Sprint tasks](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#sprint-cycle)
- [Collaborating on identifying issues to be completed within Epics](https://about.gitlab.com/handbook/marketing/growth-marketing/#epics)
- [Effective Vendor Management](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#vendor-management)


## Levels

Read more about [levels](https://about.gitlab.com/handbook/hiring/#definitions) at GitLab here.

## Junior Frontend Engineer

Junior Frontend Engineers, while sharing the same requirements and responsibilities outlined above, typically join with less or alternate experience than typical Frontend Engineers.

## Intermediate Frontend Engineer

Intermediate Frontend Engineers are expected to meet the requirements and execute the responsibilities with minimal assistance.

## Senior Frontend Engineer

The Senior Frontend Engineer role extends the [Frontend Engineer](https://about.gitlab.com/job-families/engineering/development/frontend/#requirements) role.

## Responsibilities

- Work with cross functional partners, acting as a team leader.
- Critical decision making, and knowing what will have the biggest business impact when prioritizing.
- Advocate for improvements to Marketing website quality, security, and performance that have particular impact across your team.
- Experience with performance and optimization problems, particularly at large scale, and a demonstrated ability to both diagnose and prevent these problems
- Help to define and improve our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
- Provide mentorship for Junior and Intermediate Engineers in your section to help them grow in their technical responsibilities and remove blockers.
- Consistently ship moderately sized features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

## Career Ladder

The next step in the Frontend Engineer - Marketing job family is not yet defined at GitLab. 

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, you can find their job title on our [team page](/company/team).

* Select candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with our Senior Manager, Digital Experience. 
* Next, candidates will be invited to schedule a 30 minute technical interview with our Marketing Full Stack Developer and an interview with a Senior Brand Designer. 
* Next, candidates will be invited to schedule a 30 minute interview with the Senior Director, Growth Marketing. 
* Finally, candidates will be invited to schedule a 30 minute follow up interview with the Senior Manager, Digital Experience. 
* Successful candidates will subsequently be made an offer via phone or video. 

Additional details about our process can be found on our [hiring page](/handbook/hiring).