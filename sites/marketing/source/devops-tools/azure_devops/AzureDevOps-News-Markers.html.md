---
layout: markdown_page
title: "Azure DevOps News Markers"
description: "Azure DevOps News Markers - news from a variety of sources."
canonical_path: "/devops-tools/azure_devops/AzureDevOps-News-Markers.html"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

**Chatter on Azure DevOps**
   * Microsoft is pulling people from Azure DevOps and moving them to Github.  [GitHub’s SVP of Engineering](https://www.linkedin.com/in/keithba/) was formerly responsible for Azure DevOps.

**Regarding the rename and repackaging of VSTS to Azure DevOps:**
   * Microsoft customers wanted the company to break up the Visual Studio Team Services (VSTS) platform so they could choose individual services, said Jamie Cool, Microsoft's program manager for Azure DevOps. By doing so, the company also hopes to attract a wider audience that includes Mac and Linux developers, as well as open source developers in general, who avoid Visual Studio, Microsoft's flagship development tool set.

**Lots of emphasis on cross platform (windows, Mac, Linux), and free macOS CI/CD is pretty rare.**

**All paid plans include unlimited stakeholder users who can view and contribute to work items and boards, and view dashboards, charts, and pipelines**

**From [https://azure.microsoft.com/en-us/blog/introducing-azure-devops/](https://azure.microsoft.com/en-us/blog/introducing-azure-devops/)**
      * Azure DevOps represents the evolution of Visual Studio Team Services (VSTS). VSTS users will be upgraded into Azure DevOps projects automatically. For existing users, there is no loss of functionally, simply more choice and control. The end to end traceability and integration that has been the hallmark of VSTS is all there. Azure DevOps services work great together.

   * As part of this change, the services have an updated user experience.
   * Users of the on-premises Team Foundation Server (TFS) will continue to receive updates based on features live in Azure DevOps. Starting with next version of TFS, the product will be called Azure DevOps Server and will continue to be enhanced through our normal cadence of updates.
      
**[HackerNews comments saying it's just a rebrand](https://news.ycombinator.com/item?id=17952273) - PM for AzureDevOps responding:**
      * PM for Azure DevOps here (formerly VSTS). It is a rebranding, but it's more than merely a rebranding. We're breaking out the individual services so that they're easier to adopt. For example, if you're just interested in pipelines, you can adopt only pipelines.

**From a call with a prospect Bank:**
   * Went with Azure DevOps because: It's platform agnostic, it's in the cloud, great capabilityality, tons of functionality, it does what we need it to do. We like it a lot. It really has nothing to do with Microsoft. Microsoft is very agnostic and open source embracing now, so that the old Java vs .Net thing is kind of over.
   * Appealed to a shop that was "more Java than Microsoft technologies". But they had lots of the Microsoft development suite already, and trusted where Microsoft is going.
   * Azure DevOps is dropping new releases every sprint (2-3 weeks). Their roadmap is public: [Azure DevOps public Roadmap and Release History](https://docs.microsoft.com/en-us/azure/devops/release-notes/)

**What does the migration path look like from Azure DevOps to GitLab for SCM and CI/CD??**
   * SCM migration - Available tools to migrate without losing repo history. Like with any product, centrally managed -> Git takes training.
   * CI/CD migration - A little tougher. Most existing will be using build pipelines and release pipelines created through UI. Conversion is task by task reconstruction of pipelines, and potential combining into one on GitLab. Azure DevOps pipeline as code is new, although Microsoft is starting to push it as the default now.

**[A developers experience and opinions using Microsoft Devops CI](https://toxicbakery.github.io/vsts-devops/microsoft-devops-ci/)**

**[Hacker News: Microsoft’s Azure DevOps: An Unsatisfying Adventure](https://news.ycombinator.com/item?id=18983586)**
